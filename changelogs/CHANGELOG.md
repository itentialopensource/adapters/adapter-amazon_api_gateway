
## 0.5.1 [07-14-2022]

* patch/touchups - touchups to adapter.js

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!7

---

## 0.5.0 [07-12-2022]

- Changes for STS Auth, IAM and Migration
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!6

---

## 0.4.5 [02-26-2021] & 0.4.4 [02-26-2021]

- Migration to bring up to the latest foundation

- Script handles
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity

- Manual Work
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!5

---

## 0.4.3 [07-02-2020]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!4

---

## 0.4.2 [01-16-2020] & 0.4.1 [12-31-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!3

---

## 0.4.0 [11-07-2019]

- Update the adapter to the latest adapter foundation.
	- Updating to adapter-utils 4.24.3 (automatic)
	- Add sample token schemas (manual)
	- Adding placement property to getToken response schema (manual - before encrypt)
	- Adding sso default into action.json for getToken (manual - before response object)
	- Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
	- Update sample properties to include new properties (manual)
	- Update integration test for raw mockdata (automatic)
	- Update test properties (manual)
	- Changes to artifactize (automatic)
	- Update type in sampleProperties so it is correct for the adapter (manual)
	- Update the readme (automatic)

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!2

---

## 0.3.0 [09-12-2019] & 0.2.0 [09-10-2019]

- Mirgate to the latest foundation -- this is a breaking change as it removed addlHeaders from method signatures but that is best practice and this is a pre-release adapter.

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!1

---

## 0.1.1 [08-19-2019]

- Initial Commit

See commit a72e23d

---
