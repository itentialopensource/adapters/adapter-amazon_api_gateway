
## 0.7.8 [11-12-2024]

* more auth changes

See merge request itentialopensource/adapters/adapter-amazon_api_gateway!26

---

## 0.7.7 [10-15-2024]

* Changes made at 2024.10.14_21:19PM

See merge request itentialopensource/adapters/adapter-amazon_api_gateway!25

---

## 0.7.6 [09-30-2024]

* update auth docs

See merge request itentialopensource/adapters/adapter-amazon_api_gateway!23

---

## 0.7.5 [09-12-2024]

* add properties for sts

See merge request itentialopensource/adapters/adapter-amazon_api_gateway!22

---

## 0.7.4 [08-22-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-amazon_api_gateway!21

---

## 0.7.3 [08-14-2024]

* Changes made at 2024.08.14_19:35PM

See merge request itentialopensource/adapters/adapter-amazon_api_gateway!20

---

## 0.7.2 [08-07-2024]

* Changes made at 2024.08.06_21:38PM

See merge request itentialopensource/adapters/adapter-amazon_api_gateway!19

---

## 0.7.1 [08-05-2024]

* Changes made at 2024.08.05_19:15PM

See merge request itentialopensource/adapters/adapter-amazon_api_gateway!18

---

## 0.7.0 [05-16-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!17

---

## 0.6.8 [03-27-2024]

* Changes made at 2024.03.27_13:52PM

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!16

---

## 0.6.7 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!15

---

## 0.6.6 [03-13-2024]

* Changes made at 2024.03.13_11:44AM

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!14

---

## 0.6.5 [03-11-2024]

* Changes made at 2024.03.11_16:20PM

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!13

---

## 0.6.4 [02-27-2024]

* Changes made at 2024.02.27_11:56AM

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!12

---

## 0.6.3 [01-27-2024]

* dyanmic region support in sts params

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!11

---

## 0.6.2 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!10

---

## 0.6.1 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!9

---

## 0.6.0 [11-10-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-amazon_api_gateway!8

---
