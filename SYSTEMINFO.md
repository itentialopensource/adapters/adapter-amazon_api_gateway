# Amazon API Gateway

Vendor: Amazon Web Services
Homepage: https://aws.amazon.com/

Product: API Gateway
Product Page: https://aws.amazon.com/api-gateway/

## Introduction
We classify AWS API Gateway into the Security/SASE domain as AWS API Gateway provides network security features.

## Why Integrate
The AWS API Gateway adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS API Gateway to that enable developers to create, publish, maintain, monitor, and secure APIs at any scale.

With this adapter you have the ability to perform operations with AWS API Gateway such as:

- Get Domain Names
- Create Domain Name
- Get Route
- Update Route
- Get Deployment
- Tag Resource

## Additional Product Documentation
The [API documents for AWS API Gateway](https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-swagger-extensions.html)
