## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Amazon API Gateway. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Amazon API Gateway.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Amazon API Gateway. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getApisSTSRole(maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets a collection of Api resources.</td>
    <td style="padding:15px">{base_path}/{version}/apis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApiSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an Api resource.</td>
    <td style="padding:15px">{base_path}/{version}/apis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiSTSRole(apiId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes an Api resource.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiSTSRole(apiId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets an Api resource.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApiSTSRole(apiId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates an Api resource.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthorizersSTSRole(apiId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the Authorizers for an API.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/authorizers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthorizerSTSRole(apiId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an Authorizer for an API.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/authorizers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthorizerSTSRole(apiId, authorizerId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes an Authorizer.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/authorizers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthorizerSTSRole(apiId, authorizerId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets an Authorizer.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/authorizers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAuthorizerSTSRole(apiId, authorizerId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates an Authorizer.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/authorizers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeploymentsSTSRole(apiId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the Deployments for an API.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeploymentSTSRole(apiId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a Deployment for an API.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeploymentSTSRole(apiId, deploymentId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a Deployment.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeploymentSTSRole(apiId, deploymentId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets a Deployment.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeploymentSTSRole(apiId, deploymentId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates a Deployment.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrationsSTSRole(apiId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the Integrations for an API.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/integrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIntegrationSTSRole(apiId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an Integration.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/integrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntegrationSTSRole(apiId, integrationId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes an Integration.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/integrations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrationSTSRole(apiId, integrationId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets an Integration.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/integrations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIntegrationSTSRole(apiId, integrationId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates an Integration.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/integrations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrationResponsesSTSRole(apiId, integrationId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the IntegrationResponses for an Integration.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/integrations/{pathv2}/integrationresponses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIntegrationResponseSTSRole(apiId, integrationId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an IntegrationResponses.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/integrations/{pathv2}/integrationresponses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntegrationResponseSTSRole(apiId, integrationId, integrationResponseId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes an IntegrationResponses.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/integrations/{pathv2}/integrationresponses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrationResponseSTSRole(apiId, integrationId, integrationResponseId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets an IntegrationResponses.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/integrations/{pathv2}/integrationresponses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIntegrationResponseSTSRole(apiId, integrationId, integrationResponseId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates an IntegrationResponses.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/integrations/{pathv2}/integrationresponses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getModelsSTSRole(apiId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the Models for an API.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/models?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createModelSTSRole(apiId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a Model for an API.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/models?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteModelSTSRole(apiId, modelId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a Model.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/models/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getModelSTSRole(apiId, modelId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets a Model.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/models/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateModelSTSRole(apiId, modelId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates a Model.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/models/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getModelTemplateSTSRole(apiId, modelId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets a model template.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/models/{pathv2}/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutesSTSRole(apiId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the Routes for an API.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRouteSTSRole(apiId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a Route for an API.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRouteSTSRole(apiId, routeId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a Route.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/routes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouteSTSRole(apiId, routeId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets a Route.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/routes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRouteSTSRole(apiId, routeId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates a Route.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/routes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouteResponsesSTSRole(apiId, maxResults, nextToken, routeId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the RouteResponses for a Route.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/routes/{pathv2}/routeresponses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRouteResponseSTSRole(apiId, routeId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a RouteResponse for a Route.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/routes/{pathv2}/routeresponses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRouteResponseSTSRole(apiId, routeId, routeResponseId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a RouteResponse.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/routes/{pathv2}/routeresponses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouteResponseSTSRole(apiId, routeId, routeResponseId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets a RouteResponse.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/routes/{pathv2}/routeresponses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRouteResponseSTSRole(apiId, routeId, routeResponseId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates a RouteResponse.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/routes/{pathv2}/routeresponses/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStagesSTSRole(apiId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the Stages for an API.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/stages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStageSTSRole(apiId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a Stage for an API.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/stages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStageSTSRole(apiId, stageName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a Stage.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/stages/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStageSTSRole(apiId, stageName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets a Stage.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/stages/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateStageSTSRole(apiId, stageName, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates a Stage.</td>
    <td style="padding:15px">{base_path}/{version}/apis/{pathv1}/stages/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNamesSTSRole(maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the domain names for an AWS account.</td>
    <td style="padding:15px">{base_path}/{version}/domainnames?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDomainNameSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a domain name.</td>
    <td style="padding:15px">{base_path}/{version}/domainnames?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDomainNameSTSRole(domainName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a domain name.</td>
    <td style="padding:15px">{base_path}/{version}/domainnames/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameSTSRole(domainName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets a domain name.</td>
    <td style="padding:15px">{base_path}/{version}/domainnames/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDomainNameSTSRole(domainName, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates a domain name.</td>
    <td style="padding:15px">{base_path}/{version}/domainnames/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiMappingsSTSRole(domainName, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">The API mappings.</td>
    <td style="padding:15px">{base_path}/{version}/domainnames/{pathv1}/apimappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApiMappingSTSRole(domainName, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an API mapping.</td>
    <td style="padding:15px">{base_path}/{version}/domainnames/{pathv1}/apimappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiMappingSTSRole(apiMappingId, domainName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes an API mapping.</td>
    <td style="padding:15px">{base_path}/{version}/domainnames/{pathv1}/apimappings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiMappingSTSRole(apiMappingId, domainName, stsParams, roleName, callback)</td>
    <td style="padding:15px">The API mapping.</td>
    <td style="padding:15px">{base_path}/{version}/domainnames/{pathv1}/apimappings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApiMappingSTSRole(apiMappingId, domainName, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">The API mapping.</td>
    <td style="padding:15px">{base_path}/{version}/domainnames/{pathv1}/apimappings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagsSTSRole(resourceArn, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the Tags for an API.</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tagResourceSTSRole(resourceArn, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Tag an APIGW resource</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">untagResourceSTSRole(resourceArn, tagKeys, stsParams, roleName, callback)</td>
    <td style="padding:15px">Untag an APIGW resource</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
